﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HPCounter : MonoBehaviour
{

    
    public static float hp = 3;
    
    Text hpText;
    
        void Start()
    {
        
        hpText = GetComponent<Text> ();
    }
    void Update()
    {
        
        hpText.text = "HP: " + hp;
        
    }
}
