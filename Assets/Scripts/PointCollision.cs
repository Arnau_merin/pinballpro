﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointCollision : MonoBehaviour
{
    
   private Rigidbody2D rb2d; 
    
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D> ();
        
    }
   void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bouncer"){
            PointCounter.points++;
            //Debug.Log(points);
        }
        if (collision.gameObject.tag == "Player"){
            gameObject.GetComponent<Renderer> ().material.color = Color.blue;
            //Debug.Log(points);
        }
        else{
            gameObject.GetComponent<Renderer> ().material.color = Color.red;
        }

        if (collision.gameObject.tag == "Reset"){
            HPCounter.hp--;
            if(HPCounter.hp > 0){
                Vector3 Restart = new Vector3(19.5f, 0f, 9.5f);
                transform.position = Restart;
            }
            else{
                Debug.Log("JANDU PLS NO QUIERO SUSPENDER");
            }
            //Debug.Log(points);
        }
    }
}
