﻿ using System.Collections.Generic;
   using UnityEngine;
   using UnityEngine.UI;
 
   public class Bouncing : MonoBehaviour {
    [SerializeField] Sprite myFirstImage, mySecondImage;
   Image myImageComponent;
   
   void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player"){
             SetImage1();
            
        }
        else{
             SetImage2();
        }
    }
 
   void Start() //Lets start by getting a reference to our image component.
   {
        Renderer rend = GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Specular");
        rend.material.SetColor("_SpecColor", Color.red);
       //myImageComponent = GetComponent<Image>(); //Our image component is the one attached to this gameObject.
       //myImageComponent.sprite = myFirstImage;
   }
 
   public void SetImage1() //method to set our first image
   {
      //gameObject.GetComponent<Image>().sprite = myFirstImage;
      // myImageComponent.sprite = myFirstImage;
   }
 
   public void SetImage2()
   {
       //gameObject.GetComponent<Image>().sprite = mySecondImage;
       //myImageComponent.sprite = mySecondImage;
   }
}